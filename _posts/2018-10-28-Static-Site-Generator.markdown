---
layout: post
title:  "Static Site Generator!"
date:   2018-10-28 07:29:27 +0700
categories: Static Site
---

Dalam artikel ini saya akan membahas mengenai static site generator. Situs web statis berisi halaman Web dengan konten tetap. Setiap halaman dikodekan dalam HTML dan menampilkan informasi yang sama untuk setiap pengunjung. Situs statis adalah jenis situs web yang paling dasar dan paling mudah dibuat. Tidak seperti situs web dinamis , mereka tidak memerlukan pemrograman Web atau desain database . Situs statis dapat dibuat hanya dengan membuat beberapa halaman HTML dan menerbitkannya ke server Web.

Karena halaman Web statis berisi kode tetap, konten setiap halaman tidak berubah kecuali diperbarui secara manual oleh webmaster . Ini berfungsi baik untuk situs web kecil, dapat juga membuat situs besar dengan ratusan atau ribuan laman, akan tetapi biasanya sulit dikelola. Oleh karena itu, situs web yang lebih besar biasanya menggunakan halaman dinamis, yang dapat diperbarui hanya dengan memodifikasi catatan basis data. Situs statis yang berisi banyak halaman seringkali dirancang menggunakan templat . Ini memungkinkan untuk memperbarui beberapa halaman sekaligus, dan juga membantu menyediakan tata letak yang konsisten di seluruh situs.

Mengapa menggunakan static site generator ?

Diantara banyak alasan, kecepatan dan keamanan menjadi suatu kebutuhan bagi semua pengguna web atau blog, baik bagi sang pemilik ataupun pembacanya.

Static site generator memperhatikan kelemahan yang ada pada sistem database. Sepertinya setiap hari kita mendengar tentang serangan situs monolitik gaya lama. Secara teori, situs statis lebih aman; situs prebuilt dan disajikan tanpa kode server berjalan langsung di situs anda. Isolasi ini menghilangkan kesempatan bagi hacker untuk menyusup ke pipeline anda. Oleh karena itu mengapa pengembang, lembaga dan produsen konten web semakin banyak yang menggunakan Generator website statis.

Dengan browser modern, situs-situs yang dibangun dengan JavaScript, API dan Markup menawarkan kemampuan untuk melayani konten yang sangat dinamis tanpa belenggu backend standar, kelambatan yang menyakitkan (dan mahal) (database dan server). .

Apps monolitik menjalankan kode sisi server setiap kali seorang pengunjung membuat permintaan. Ini membuang-buang waktu yang berharga dan membuka lubang keamanan situs Anda. Dengan menggunakan static site generator dan atau static site generator CMS, flat file disajikan dari CDNs di seluruh dunia, meningkatkan kecepatan dan uptime, mudah memanfaatkan sistem kontrol versi seperti Git, proses pembuatan dan memperbarui situs jauh lebih terintegrasi dengan alur kerja pengembang.

Tentu saja, ketika anda ingin mencari static site generator untuk beralih, berbagai pilihan dapat membingungkan. Untuk itu dalam artikel ini membahas mengenai beberapa static site generator, yang bisa anda pilih sesuai dengan ekspektasi yang anda inginkan dan sesuai dengan keahlian dalam bahasa pemrograman yang anda miliki.

1 Jekyll

Jekyll adalah salah satu generator statis situs paling populer. Ini tidak mengejutkan, mengingat dibuat dan fantastis didukung Github

Jekyll dibangun dalam bahasa pemrograman Ruby dan biasanya digunakan untuk blog atau proyek pribadi. Jekyll mengambil sebuah direktori yang berisi file teks, menuliskan bahwa konten dengan Markdown dan template, dan menghasilkan website statis yang siap dipublikasikan. Komunitas besar dan berbagai macam plugin yang membuatnya melompat jauh ke tempat para blogger yang datang dari dunia WordPress dan Drupal, sehingga mudah untuk mengimpor konten dari format tersebut dan banyak lagi.